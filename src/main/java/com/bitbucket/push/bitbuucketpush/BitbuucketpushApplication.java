package com.bitbucket.push.bitbuucketpush;

import com.bitbucket.push.bitbuucketpush.config.ChannelDefinitions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableBinding({ChannelDefinitions.class})
public class BitbuucketpushApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitbuucketpushApplication.class, args);
	}

}
