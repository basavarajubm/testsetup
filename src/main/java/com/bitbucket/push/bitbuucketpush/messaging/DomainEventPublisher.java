package com.bitbucket.push.bitbuucketpush.messaging;

import com.bitbucket.push.bitbuucketpush.config.ChannelDefinitions;
import com.bitbucket.push.bitbuucketpush.event.Demoevent;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class DomainEventPublisher {

    private MessageChannel messageChannel;

    public DomainEventPublisher(ChannelDefinitions channelDefinitions) {
        this.messageChannel = channelDefinitions.outgoingChannel();
    }

    public void publish(Demoevent demoevent){
        messageChannel.send(MessageBuilder.withPayload(demoevent).build());
    }
}
