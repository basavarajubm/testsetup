package com.bitbucket.push.bitbuucketpush.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Demoevent {

    private String id;
    private String name;
}
