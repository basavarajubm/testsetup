package com.bitbucket.push.bitbuucketpush.web;

import com.H2H.queue.web.api.QueueApi;
import com.bitbucket.push.bitbuucketpush.event.Demoevent;
import com.bitbucket.push.bitbuucketpush.messaging.DomainEventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class DemoController implements QueueApi {

    private final DomainEventPublisher domainEventPublisher;

    public ResponseEntity<String> puttoqueue() {
        domainEventPublisher.publish(new Demoevent("1","First Id"));
        return ResponseEntity.ok("Posted in queue");
    }
}
