package com.bitbucket.push.bitbuucketpush.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface ChannelDefinitions {

    String CHANNEL_NAME="aws-demo-channel";

    @Output(CHANNEL_NAME)
    MessageChannel outgoingChannel();

}
